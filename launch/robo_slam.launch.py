from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():
    """
    ROS 2 launch files are special Python files that contain a function named
    generate_launch_description. This function must return a LaunchDescription
    object that represents the sets of actions that ROS should execute to start
    the system
    """

    return LaunchDescription(
        [
            Node(
                package="robo_slam",
                executable="robo_slam",
                output="screen",
            ),
            Node(
                package="robo_slam",
                executable="robo_localization",
                parameters=[
                    {"num_particles": 300},
                    {"percent_particles_to_regenerate": 0.01},
                    {"odometry_topic": "/robo1/odom"},
                    {"laser_scan_topic": "/robo1/laser_scanner"},
                    {"sensor_fusion_slop": 0.1},
                    {
                        "occupancy_grid_csv": "/home/usi/dev_ws/src/robo_slam/robo_slam/occupancy_grid.csv"
                    },
                ],
                output="screen",
            ),
            Node(
                package="robo_slam",
                executable="robo_localization_visualizer",
                output="screen",
            ),
            Node(
                package="robo_slam",
                executable="robo_localization_estimation",
                output="screen",
            ),
        ]
    )
