from typing import List, Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt
import pandas as pd
import seaborn as sns
from numpy.random import random
from shapely.geometry import LineString
from shapely.ops import unary_union
from scipy.stats import norm
import time
from geometry_msgs.msg import Pose2D

from robo_utils import (
    euclidean_distance,
    get_transformation,
    occupancy_grid_to_scatter,
    mktr,
    mkrot,
    homogeneous_to_euclidean,
)


def gen_random_particle(
    dw: int, dh: int, occupancy_grid: np.ndarray = None
) -> Tuple[float, float, float]:
    """
    Generate random particle

    Args:
        dw (int): width of occupancy grid
        dh (int): height of occupancy grid
        occupancy_grid (np.ndarray): if supplied, will generate a particle that is not in an occupied cell

    Returns:
        (int, int, int) : (x, y, heading)
    """
    theta = random() * 2 * np.pi
    while True:
        x = int(random() * dw)
        y = int(random() * dh)
        if occupancy_grid is None or occupancy_grid[x, y] == 0:
            break

    return (x, y, theta)


def plot_occupancy_grid(occupancy_grid: np.ndarray):
    df_scatter = occupancy_grid_to_scatter(occupancy_grid)
    sns.scatterplot(
        data=df_scatter, x="x", y="y", hue="hue", palette=["black"], legend=False
    )


def plot_particle(
    x: float, y: float, theta: float, occupancy_grid: np.ndarray, color="red", size=50
):
    line_length = 40
    x1 = x + line_length * np.cos(theta)
    y1 = y + line_length * np.sin(theta)

    plt.scatter(x, y, c=color, s=size)
    sns.lineplot(x=[x, x1], y=[y, y1], color=color)


def detect_closest_obstacle(
    x: int, y: int, theta: float, occupancy_grid: np.ndarray
) -> Optional[Tuple[int, int]]:
    dw = occupancy_grid.shape[0]
    dh = occupancy_grid.shape[1]

    # take step along heading so we can calculate slope
    x1 = x + np.cos(theta)
    y1 = y + np.sin(theta)
    m = (y1 - y) / (x1 - x)
    m_abs = np.abs(m)

    # determine which direction our x and y steps should be
    x_dir = 1 if x1 > x else -1
    y_dir = 1 if y1 > y else -1

    # generate steps to the border of the map
    #   using - 1 because np.arange generates inclusive indices when step is a float type.
    max_x = dw - 1 if x_dir == 1 else 0
    max_y = dh - 1 if y_dir == 1 else 0

    # generate steps along the line
    if m_abs < 1:
        xrange = np.arange(x, max_x, x_dir * 1)
        yrange = np.arange(y, max_y, y_dir * m_abs).round().astype(int)
    else:
        xrange = np.arange(x, max_x, x_dir * (1 / m_abs)).round().astype(int)
        yrange = np.arange(y, max_y, y_dir * 1)

    for x_i, y_i in zip(xrange, yrange):
        # plt.scatter(x_i, y_i, c="b", s=1)
        if occupancy_grid[x_i, y_i] > 0:
            return (x_i, y_i)

    return None


# def detect_closest_obstacle(
#    x: float, y: float, theta: float, occupancy_grid: np.ndarray
# ):
#    dw = occupancy_grid.shape[0]
#    dh = occupancy_grid.shape[1]
#
#    # max possible distance to obstacle would be the diagonal of the grid
#    max_distance = np.sqrt(dw**2 + dh**2)
#    x1 = int(x + max_distance * np.cos(theta))
#    y1 = int(y + max_distance * np.sin(theta))
#
#    # todo: should really calculate delta distance based on grid granularity
#    #       but grid granularity is 1, so this should work fine. We may double
#    #       check some grids, but we shouldn't miss any
#    step = 1
#    line = LineString(([x, y], [x1, y1]))
#    distances = np.arange(0, line.length, step)
#    points = [line.interpolate(distance) for distance in distances]
#
#    for point in points:
#        x_i = int(point.x)
#        y_i = int(point.y)
#        # stop if we're already off the map
#        if x_i < 0 or x_i >= dw or y_i < 0 or y_i >= dh:
#            break
#
#        # otherwise stop if we reach an obstacle
#        if occupancy_grid[x_i, y_i] > 0:
#            return (x_i, y_i)
#
#    return None


def plot_obstacle(obstacle: Optional[np.ndarray], color="blue"):
    if obstacle is not None:
        xd, yd = obstacle
        plt.scatter(xd, yd, c=color, s=25)


def calculate_probabilities(particle_range_measurements, robot_range_measurement):
    # 5 cm standard deviation
    std = 5
    range_norm = norm(robot_range_measurement, std)

    new_beliefs = range_norm.pdf(particle_range_measurements)

    return new_beliefs / sum(new_beliefs)


def resample(beliefs, particles):
    num_samples_to_keep = int(len(particles) * 0.1)

    # reshape the beliefs array so each element is an accumulation of all the probabilities up to that point
    probability_sum = 0
    beliefs_accumulated = np.zeros(beliefs.shape)
    for i in range(len(beliefs)):
        probability_sum += beliefs_accumulated[i]
        beliefs_accumulated[i] = probability_sum

    # sample from the accumulated probabilities
    # using low variance resampling (see figure 5 here http://www.cs.cmu.edu/~16831-f14/notes/F11/16831_lecture04_tianyul.pdf)
    samples_to_take: List[float] = list(
        np.arange(np.random.random() / num_samples_to_keep, 1, 1 / num_samples_to_keep)
    )
    belief_to_find = samples_to_take.pop()
    particles_to_keep = []
    for i, belief in enumerate(beliefs):
        if belief >= belief_to_find:
            particles_to_keep.append(particles[i])
            if len(samples_to_take) > 0:
                belief_to_find = samples_to_take.pop()
            else:
                break

    return


def initialize_probabilities(num_particles: int) -> npt.NDArray[np.float64]:
    """
    Each particle with start with an equal probability

    Args:
        num_particles (int): number of particles to initialize

    Returns:
        np.ndarray: probability matrix
    """
    return np.ones((num_particles,)) / num_particles


def distance(
    location: Tuple[float, float], obstacle: Optional[Tuple[float, float]]
) -> float:
    if obstacle is None:
        return -1

    return np.sqrt((location[0] - obstacle[0]) ** 2 + (location[1] - obstacle[1]) ** 2)


def main():
    num_particles = 100

    occupancy_grid = np.genfromtxt("occupancy_grid.csv", delimiter=",", dtype=np.uint8)
    dw = occupancy_grid.shape[0]
    dh = occupancy_grid.shape[1]

    path = "/home/usi/dev_ws/src/robo_slam/robo_slam/"
    occupancy_grid_csv = path + "occupancy_grid.csv"
    occupancy_grid = np.genfromtxt(occupancy_grid_csv, delimiter=",", dtype=np.uint8)

    # plotting occupancy grid
    plot_occupancy_grid(occupancy_grid)

    # generating random particle for our robot
    xr, yr, thetar = gen_random_particle(dw, dh, occupancy_grid)

    plot_particle(xr, yr, thetar, occupancy_grid, "green")
    # pose1 = Pose2D()
    # pose1.x = float(xr)
    # pose1.y = float(yr)
    # pose1.theta = float(thetar)

    print(f"Starting pose is: {xr}, {yr}, {thetar}")
    transformation = mktr(10, 10) @ mkrot(np.deg2rad(45))
    coords = homogeneous_to_euclidean(mktr(xr, yr) @ mkrot(thetar) @ transformation)
    print(f"Ending pose is: {coords[0]}, {coords[1]}, {coords[2]}")
    plot_particle(coords[0], coords[1], coords[2], occupancy_grid, "red")
    plt.show()

    # robstacle = detect_closest_obstacle(xr, yr, thetar, occupancy_grid)
    # rmeasure = distance((xr, yr), robstacle)
    ## plot_obstacle(obstacler, "green")

    ## generating random particles as estimates of where our actual robot is
    # particles = [gen_random_particle(dw, dh) for _ in range(num_particles)]

    # tstart = time.time()
    ## taking range measurements for each particle
    # range_measurements = np.asarray(
    #    [
    #        distance((x, y), detect_closest_obstacle(x, y, theta, occupancy_grid))
    #        for x, y, theta in particles
    #    ]
    # )
    # tend = time.time()
    # print(tend - tstart)

    # belief = initialize_probabilities(num_particles)
    # new_belief = calculate_probabilities(range_measurements, rmeasure)

    # plot_particles = np.array(particles)
    # for (x, y, theta), belief in zip(plot_particles, 100 * new_belief):
    #    plot_particle(x, y, theta, occupancy_grid, "red", belief)

    # plt.show()
    # plt.scatter(
    #    x=plot_particles[:, 0], y=plot_particles[:, 1], c="red", s=100 * new_belief
    # )

    #    for i in range(num_particles):
    #        # generate random particle with a random heading
    #        x, y, theta = gen_random_particle(dw, dh)
    #
    #        # detect closest obstacle along its path
    #        obstacle = detect_closest_obstacle(x, y, theta, occupancy_grid)
    #
    #        # particle and point where obstacle is detected
    #        plot_particle(x, y, theta, occupancy_grid)
    #        plot_obstacle(obstacle)
    #


if __name__ == "__main__":
    main()

#### pseudocode
# 1. generate random particles and calculate a probability for each particle
#    based on a simulation of a range measurement from its pose
# 2. move robot, use dead reckoning to track movement. Take range measurement.
# 3. for each particle:
#        a. update particle based on movement
#        b. update particles simulated range measurement for its new pose
#        c. calculate a new probability for each particle based on its new pose
#           and simulated range measurement
# 4. resample. Kill off particles with low probability and generate new particles
#    near the high probability particles
