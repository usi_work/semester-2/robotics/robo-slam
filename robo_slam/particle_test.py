from typing import Optional, Tuple
import numpy as np
import pandas as pd
import seaborn as sns
from numpy.random import random
from numpy.random import default_rng
import matplotlib.pyplot as plt
import time

from shapely.geometry import LineString
from shapely.ops import unary_union

from robo_utils import occupancy_grid_to_scatter

rng = default_rng(42)


def gen_random_particle(
    dw: int, dh: int, occupancy_grid: np.ndarray = None
) -> Tuple[float, float, float]:
    """
    Generate random particle

    Args:
        dw (int): width of occupancy grid
        dh (int): height of occupancy grid
        occupancy_grid (np.ndarray): if supplied, will generate a particle that is not in an occupied cell

    Returns:
        (int, int, int) : (x, y, heading)
    """
    theta = random() * 2 * np.pi
    while True:
        x = int(random() * dw)
        y = int(random() * dh)
        if occupancy_grid is None or occupancy_grid[x, y] == 0:
            break

    return (x, y, theta)


def plot_occupancy_grid(occupancy_grid: np.ndarray):
    df_scatter = occupancy_grid_to_scatter(occupancy_grid)
    sns.scatterplot(
        data=df_scatter, x="x", y="y", hue="hue", palette=["black"], legend=False
    )


def plot_particle(
    x: float, y: float, theta: float, occupancy_grid: np.ndarray, color="red"
):
    line_length = 40
    x1 = x + line_length * np.cos(theta)
    y1 = y + line_length * np.sin(theta)

    plt.scatter(x, y, c=color, s=50)
    sns.lineplot(x=[x, x1], y=[y, y1], color=color)


def detect_closest_obstacle(
    x: int, y: int, theta: float, occupancy_grid: np.ndarray
) -> Optional[Tuple[int, int]]:
    dw = occupancy_grid.shape[0]
    dh = occupancy_grid.shape[1]

    # take step along heading so we can calculate slope
    x1 = x + np.cos(theta)
    y1 = y + np.sin(theta)
    m = (y1 - y) / (x1 - x)
    m_abs = np.abs(m)

    # determine which direction our x and y steps should be
    x_dir = 1 if x1 > x else -1
    y_dir = 1 if y1 > y else -1

    # generate steps to the border of the map
    #   using - 1 because np.arange generates inclusive indices when step is a float type.
    max_x = dw - 1 if x_dir == 1 else 0
    max_y = dh - 1 if y_dir == 1 else 0

    # generate steps along the line
    if m_abs < 1:
        xrange = np.arange(x, max_x, x_dir * 1)
        yrange = np.arange(y, max_y, y_dir * m_abs).round().astype(int)
    else:
        xrange = np.arange(x, max_x, x_dir * (1 / m_abs)).round().astype(int)
        yrange = np.arange(y, max_y, y_dir * 1)

    for x_i, y_i in zip(xrange, yrange):
        plt.scatter(x_i, y_i, c="g", s=1)
        if occupancy_grid[x_i, y_i] > 0:
            return (x_i, y_i)

    return None


def plot_obstacle(obstacle: Optional[np.ndarray], color="blue"):
    if obstacle is not None:
        xd, yd = obstacle
        plt.scatter(xd, yd, c=color, s=25)


def main():
    num_particles = 5

    occupancy_grid = np.genfromtxt("occupancy_grid.csv", delimiter=",", dtype=np.uint8)
    dw = occupancy_grid.shape[0]
    dh = occupancy_grid.shape[1]

    # plotting occupancy grid
    plot_occupancy_grid(occupancy_grid)

    ## generating random particle for our robot
    xr, yr, thetar = gen_random_particle(dw, dh, occupancy_grid)
    plot_particle(xr, yr, thetar, occupancy_grid, "red")
    obstacler = detect_closest_obstacle(xr, yr, thetar, occupancy_grid)
    plot_obstacle(obstacler, "green")

    # generating random particles as estimates of where our actual robot is
    for i in range(num_particles):
        # generate random particle with a random heading
        x, y, theta = gen_random_particle(dw, dh)

        # detect closest obstacle along its path
        obstacle = detect_closest_obstacle(x, y, theta, occupancy_grid)

        # particle and point where obstacle is detected
        plot_particle(x, y, theta, occupancy_grid, "blue")
        plot_obstacle(obstacle)

    plt.show()


if __name__ == "__main__":
    main()

#### pseudocode
# 1. generate random particles and calculate a probability for each particle
#    based on a simulation of a range measurement from its pose
# 2. move robot, use dead reckoning to track movement. Take range measurement.
# 2. for each particle:
#        a. update particle based on movement
#        b. update particles simulated range measurement for its new pose
#        c. calculate a new probability for each particle based on its new pose
#           and simulated range measurement
