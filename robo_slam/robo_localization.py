from typing import Optional, Tuple

import message_filters
import numpy as np
import rclpy
from geometry_msgs.msg import Pose2D
from nav_msgs.msg import Odometry
from numpy.random import default_rng
from rclpy.node import Node
from robo_interfaces.msg import Pose2DArray
from scipy.stats import norm
from sensor_msgs.msg import LaserScan

from .robo_utils import *


class RoboLocalization(Node):
    def __init__(self):
        super().__init__("robo_localization")  # type: ignore

        # parameters. The localization depends on odometry, laser scanning and a known occupancy grid
        odom_topic: str = self.declare_parameter("odometry_topic", "/robo1/odom").value
        laser_scan_topic: str = self.declare_parameter(
            "laser_scan_topic", "/robo1/laser_scanner"
        ).value
        sensor_fusion_slop: float = self.declare_parameter(
            "sensor_fusion_slop", 0.1
        ).value
        occupancy_grid_csv: str = self.declare_parameter(
            "occupancy_grid_csv",
            "/home/usi/dev_ws/src/robo_slam/robo_slam/occupancy_grid.csv",
        ).value
        self.num_particles: int = self.declare_parameter("num_particles", 300).value
        self.ppr: float = self.declare_parameter(
            "percent_particles_to_regenerate", 0.05
        ).value

        # loading occupancy grid
        self.occupancy_grid = np.genfromtxt(
            occupancy_grid_csv, delimiter=",", dtype=np.uint8
        )
        self.dw = self.occupancy_grid.shape[0]
        self.dh = self.occupancy_grid.shape[1]

        # todo: add parameter for scan and move models. not sure how to parametrize this yet..
        #      may just only support gaussian model and allow tuning of standard deviation

        odom_sub = message_filters.Subscriber(self, Odometry, odom_topic)
        laser_sub = message_filters.Subscriber(self, LaserScan, laser_scan_topic)

        # queue size to 1, only care about the latest odometry and laser sync, it's ok
        # if we lose messages
        message_filters.ApproximateTimeSynchronizer(
            [laser_sub, odom_sub], 1, sensor_fusion_slop
        ).registerCallback(self.odom_laser_sync_cb)

        # publishing all of the particles so they can be plotted and used for estimation
        self.particle_publisher = self.create_publisher(Pose2DArray, "/particles", 10)

        self.get_logger().info(
            f"Fusing readings from {odom_topic} and {laser_scan_topic} with slop {sensor_fusion_slop}"
        )

        self.rng = default_rng(42)
        self.particles = self.generate_random_particles()
        self.prev_pose: Optional[Pose2D] = None

    def generate_random_particle(self):
        theta = self.rng.random() * 2 * np.pi

        # making sure none of the random particles are generated in occupied cells
        while True:
            x = self.rng.integers(0, self.dw)
            y = self.rng.integers(0, self.dh)
            if self.occupancy_grid[x, y] == 0:
                break

        return (x, y, theta)

    def generate_random_particles(self) -> np.ndarray:
        """
        Generate random particles using a uniform distribution based on the size of the map.

        Returns:
            np.ndarray: Array of particles
        """
        return np.array(
            [self.generate_random_particle() for _ in range(self.num_particles)]
        )

    def publish_particles(self):
        """
        Publishing generated particles so they can be used for visualization and pose estimation.
        """
        particles_msg = Pose2DArray()
        particles_msg.poses = [
            Pose2D(x=p[0], y=p[1], theta=p[2]) for p in self.particles
        ]
        self.particle_publisher.publish(particles_msg)

    def odom_laser_sync_cb(self, laser_scan: LaserScan, odom: Odometry):
        """
        Time synchronized odometry and laser scan callback that will kick off one iteration
        of the MCL algorithm.

        Args:
            laser_scan (LaserScan): Sense measurement from robot.
            odom (Odometry): Movement measurement from robot.
        """
        pose = pose3d_to_2d(odom.pose.pose)

        # need at least 1 odometry measurement before we can start
        if self.prev_pose is None:
            self.prev_pose = pose
            return

        self.step(laser_scan, pose)
        self.prev_pose = pose

    def detect_closest_obstacle(
        self, x: int, y: int, theta: float
    ) -> Optional[Tuple[int, int]]:
        """_summary_

        Args:
            x (int): x position of particle
            y (int): y position of particle
            theta (float): orientation of particle

        Returns:
            Optional[Tuple[int, int]]: The x,y coordinates of the detected obstacle, or None if no obstacle is detected
        """
        # using DDA algorithm. See https://www.youtube.com/watch?v=W5P8GlaEOSI&ab_channel=AbdulBari
        # for an excellent explanation of the algorithm

        # take step along heading so we can calculate slope
        x1 = x + np.cos(theta)
        y1 = y + np.sin(theta)
        m = (y1 - y) / (x1 - x)
        m_abs = np.abs(m)

        # determine which direction our x and y steps should be
        x_dir = 1 if x1 > x else -1
        y_dir = 1 if y1 > y else -1

        # generate steps to the border of the map
        #   using - 1 because np.arange generates inclusive indices when step is a float type.
        max_x = self.dw - 1 if x_dir == 1 else 0
        max_y = self.dh - 1 if y_dir == 1 else 0

        # generate steps along the line
        if m_abs < 1:
            xrange = arange_as_ints(x, max_x, x_dir * 1)
            yrange = arange_as_ints(y, max_y, y_dir * m_abs, len(xrange))
        else:
            yrange = arange_as_ints(y, max_y, y_dir * 1)
            xrange = arange_as_ints(x, max_x, x_dir / m_abs, len(yrange))

        for x_i, y_i in zip(xrange, yrange):
            if self.occupancy_grid[x_i, y_i] > 0:
                return (x_i, y_i)

        return None

    def get_particle_range_measurements(self):
        return np.asarray(
            [
                distance(
                    (x, y),
                    self.detect_closest_obstacle(int(x), int(y), theta),
                )
                for x, y, theta in self.particles
            ]
        )

    def move(self, new_pose: Pose2D):
        """
        Move each particle based on the new odometry measurement, while adding some noise to the movement.

        Args:
            new_pose (Pose2D): Pose taken from odometry.
        """
        if self.prev_pose is None:
            self.prev_pose = new_pose
            return

        # determining how much to translate and rotate each particle based on the pose from the previous
        # iteration of the algorithm and the current iteration of the algorithm
        d = 100 * euclidean_distance(new_pose, self.prev_pose)
        thetadiff = new_pose.theta - self.prev_pose.theta
        transformation = np.eye(3) @ mktr(d, 0) @ mkrot(thetadiff)

        # apply transformation and rotation to each particle after first moving and rotating each particle a
        # tiny bit. This tiny bit of moving before hand simulates noise in odometry measurement. It's a bit
        # akward that we're first moving the particles a bit with noise then translating them rather than
        # adding the noise to the translation, but it works so I didn't change it
        moved_particles = [
            homogeneous_to_euclidean(
                mktr(
                    self.rng.normal(particle[0], 0.5), self.rng.normal(particle[1], 0.5)
                )
                @ mkrot(self.rng.normal(particle[2], 0.01))
                @ transformation
            )
            for particle in self.particles
        ]

        self.particles = moved_particles

    def eliminate(self):
        """
        Eliminate particles in invalid locations.
        """
        self.particles = [
            particle for particle in self.particles if self.is_legal(particle)
        ]

    def is_legal(self, particle: Tuple[float, float, float]) -> bool:
        """
        Check if the particle is within the map bounds.

        Args:
            particle (Tuple[float, float, float]): Particle to check.

        Returns:
            bool: True if particle is valid, False otherwise.
        """

        x, y, _ = particle

        if x < 0 or x >= self.dw:
            return False

        if y < 0 or y >= self.dh:
            return False

        # I also briefly looked at eliminating particles that find themselves in walls, but found
        # it was not necessary
        # if self.occupancy_grid[int(x), int(y)] > 0:
        #    return False

        return True

    def sense(self, range_measurement: float, particle_range_measurements: np.ndarray):
        """
        For each particle, generate a likelihood that the particle is a good guess for the
        pose of the robot given the range measurement coming from the robot.

        Args:
            range_measurement (float): Robots range measurement
            particle_range_measurements (np.ndarray): Range measurements for each particle to generate likelihoods for.
        """
        # 1cm standard deviation
        std = 1
        range_norm = norm(range_measurement, std)

        probabilities = range_norm.pdf(particle_range_measurements)
        self.probabilities = probabilities / np.sum(probabilities)

    def resample(self):
        """
        Resample the particles based on the likelihoods generated by the sensor model.
        """
        num_samples_to_regenerate = int(len(self.particles) * self.ppr)
        num_samples_to_keep = len(self.particles) - num_samples_to_regenerate

        # reshape the beliefs array so each element is an accumulation of all the probabilities up to that point
        probability_sum = 0
        beliefs_accumulated = np.zeros(self.probabilities.shape)
        for i, probability in enumerate(self.probabilities):
            probability_sum += probability
            beliefs_accumulated[i] = probability_sum

        # sample from the accumulated probabilities
        # using low variance resampling (see figure 5 here http://www.cs.cmu.edu/~16831-f14/notes/F11/16831_lecture04_tianyul.pdf)
        r = self.rng.random() * (1 / num_samples_to_keep)
        particles_to_keep = []
        index = 0
        for j in range(num_samples_to_keep):
            u = r + (j / num_samples_to_keep)
            while u > beliefs_accumulated[index]:
                index += 1

            x, y, theta = self.particles[index]
            particles_to_keep.append(np.array([x, y, theta]))

        # regenerate a random number of particles. also due to some rounding
        for i in range(
            num_samples_to_regenerate + (self.num_particles - len(particles_to_keep))
        ):
            particles_to_keep.append(self.generate_random_particle())

        self.particles = np.array(particles_to_keep)

    def step(self, laser_scan: LaserScan, pose: Pose2D):
        """
        Perform 1 iteration of the MCL algorithm.

        Args:
            laser_scan (LaserScan): Sense measurement from the robot.
            pose (Pose2D): Pose of the robot.
        """
        ranges = laser_scan.ranges
        front_laser_distance = ranges[len(ranges) // 2]

        self.move(pose)
        self.eliminate()
        self.sense(front_laser_distance, self.get_particle_range_measurements() / 100)
        self.resample()

        self.publish_particles()


def main():
    rclpy.init()

    node = RoboLocalization()

    # Keep processings events until someone manually shuts down the node
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
