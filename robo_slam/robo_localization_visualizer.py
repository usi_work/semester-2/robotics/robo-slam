import sys
import threading
from multiprocessing import Queue

import numpy as np
import rclpy
from geometry_msgs.msg import Pose, Pose2D
from PyQt5.QtWidgets import QApplication
from rclpy.node import Node
from robo_interfaces.msg import Pose2DArray

from .pyqt_localization_visualizer import PyQtLocalizationVisualizer
from .robo_utils import pose3d_to_2d


class RoboLocalizationVisualizer(Node):
    def __init__(
        self,
        queue_groundtruth: Queue,
        queue_particle: Queue,
        queue_estimate: Queue,
        occupancy_grid,
    ):
        super().__init__("robo_localization_visualizer")  # type: ignore

        self.create_subscription(Pose2DArray, "/particles", self.particles_cb, 10)
        self.create_subscription(Pose, "/robo1/groundtruth", self.groundtruth_cb, 10)
        self.create_subscription(Pose2D, "/estimate", self.estimate_cb, 10)

        self.queue_groundtruth = queue_groundtruth
        self.queue_particle = queue_particle
        self.queue_estimate = queue_estimate

        self.dw = occupancy_grid.shape[0]
        self.dh = occupancy_grid.shape[1]

        self.get_logger().info(f"Particle Visualizer Initialized")

    def estimate_cb(self, msg: Pose2D):
        x, y = msg.x, msg.y

        self.queue_estimate.put((x, y))

    def groundtruth_cb(self, msg: Pose):
        """
        Callback called when a new ground truth pose is received. This is the
        actual pose of the robot reported from Coppelia.

        Args:
            msg (Pose): pose of the robot
        """
        pose2d = pose3d_to_2d(msg)

        x, y, theta = pose2d.x * 100, pose2d.y * 100, pose2d.theta - np.pi
        x = x + self.dw / 2
        y = y + self.dh / 2

        # send ground truth to pyqt to graph
        self.queue_groundtruth.put((x, y, theta))

    def particles_cb(self, msg: Pose2DArray):
        """
        Particle position estimates

        Args:
            msg (Pose2DArray): Pose of each particle to be visualized. These are
                               the guesses that the Monte Carlo Localization algorithm has made.
        """
        poses = self.poses_to_array(msg.poses)

        # send particle poses to pyqt to graph
        self.queue_particle.put(poses)

    def poses_to_array(self, poses: Pose2DArray) -> np.ndarray:
        """
        Generate numpy array from array of poses

        Args:
            poses (Pose2DArray): _description_

        Returns:
            np.ndarray: 2D array where each row is a particle pose
                        with column 0 representing x and column 1 representing y
        """
        return np.array([[p.x, p.y, p.theta] for p in poses])


def ros_thread(
    groundtruth_queue: Queue,
    particle_queue: Queue,
    estimate_queue: Queue,
    occupancy_grid,
):
    rclpy.init()

    visualizer = RoboLocalizationVisualizer(
        groundtruth_queue, particle_queue, estimate_queue, occupancy_grid
    )

    try:
        rclpy.spin(visualizer)
    except KeyboardInterrupt:
        pass

    visualizer.destroy_node()
    rclpy.shutdown()


def main():
    # loading occupancy grid
    path = "/home/usi/dev_ws/src/robo_slam/robo_slam/"
    occupancy_grid_csv = path + "occupancy_grid.csv"
    occupancy_grid = np.genfromtxt(occupancy_grid_csv, delimiter=",", dtype=np.uint8)

    # queues shared between the ros localization visualizer node and the pyqt graph
    groundtruth_queue = Queue()
    particle_queue = Queue()
    estimate_queue = Queue()

    # set up ros thread
    rosthread = threading.Thread(
        target=ros_thread,
        args=(
            groundtruth_queue,
            particle_queue,
            estimate_queue,
            occupancy_grid,
        ),
    )

    # set up pyqt application
    pyqt_visualizer_app = QApplication(sys.argv)
    pyqt_visualizer = PyQtLocalizationVisualizer(
        groundtruth_queue, particle_queue, estimate_queue, occupancy_grid
    )
    pyqt_visualizer.show()

    # start ros thread an pyqt visualizer application
    rosthread.start()
    pyqt_visualizer_app.exec_()


if __name__ == "__main__":
    main()
