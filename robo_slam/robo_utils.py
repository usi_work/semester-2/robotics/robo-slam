from math import atan2, pow, sqrt
from typing import Optional, Tuple, Union

import numpy as np
import pandas as pd
import tf_transformations
from geometry_msgs.msg import Pose, Pose2D


def euclidean_distance(goal_pose: Pose2D, current_pose: Pose2D) -> float:
    """Euclidean distance between current pose and the goal."""
    return sqrt(
        pow((goal_pose.x - current_pose.x), 2) + pow((goal_pose.y - current_pose.y), 2)
    )


def linear_vel(goal_pose: Pose2D, current_pose: Pose2D, constant: float = 3.5) -> float:
    """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
    return constant * euclidean_distance(goal_pose, current_pose)


def angle_difference(angle1: float, angle2: float) -> float:
    return np.arctan2(np.sin(angle1 - angle2), np.cos(angle1 - angle2))


def steering_angle(goal_pose: Pose2D, current_pose: Pose2D) -> float:
    """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
    return atan2(goal_pose.y - current_pose.y, goal_pose.x - current_pose.x)


def steering_vel(goal_pose: Pose2D, current_pose: Pose2D, constant: float = 6) -> float:
    """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
    s_angle = steering_angle(goal_pose, current_pose)
    desired_angle = angle_difference(s_angle, current_pose.theta)
    return constant * desired_angle


def angular_vel(
    desired_angle: float, current_angle: float, constant: float = 6
) -> float:
    """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
    desired_angle = angle_difference(desired_angle, current_angle)
    return constant * desired_angle


def mktr(x, y):
    return np.array([[1, 0, x], [0, 1, y], [0, 0, 1]])


def mkrot(theta):
    return np.array(
        [
            [np.cos(theta), -np.sin(theta), 0],
            [np.sin(theta), np.cos(theta), 0],
            [0, 0, 1],
        ]
    )


def homogeneous_to_euclidean(pose: np.ndarray) -> np.ndarray:
    x = pose[0, 2]
    y = pose[1, 2]
    theta = np.arctan2(pose[1, 0], pose[0, 0])

    return np.array([x, y, theta])


def get_transformation(
    pose_start: Pose2D, pose_end: Pose2D
) -> Tuple[float, float, float]:
    # translation = mktr(pose_end.x - pose_start.x, pose_end.y - pose_start.y)
    # rotation = mkrot(pose_end.theta - pose_start.theta)
    xdiff = pose_end.x - pose_start.x
    ydiff = pose_end.y - pose_start.y
    thetadiff = pose_end.theta - pose_start.theta

    return xdiff, ydiff, thetadiff
    # translation = np.array([pose_end.x - pose_start.x, pose_end.y - pose_start.y])
    # rotation =
    # return translation @ rotation


def pose3d_to_2d(pose3: Pose) -> Pose2D:
    quaternion = (
        pose3.orientation.x,
        pose3.orientation.y,
        pose3.orientation.z,
        pose3.orientation.w,
    )

    roll, pitch, yaw = tf_transformations.euler_from_quaternion(quaternion)

    return Pose2D(
        x=pose3.position.x,
        y=pose3.position.y,
        theta=yaw,
    )


def distance(
    location: Tuple[float, float], obstacle: Optional[Tuple[float, float]]
) -> float:
    if obstacle is None:
        return -100.0

    return np.sqrt((location[0] - obstacle[0]) ** 2 + (location[1] - obstacle[1]) ** 2)


def occupancy_grid_to_scatter(occupancy_grid: np.ndarray) -> pd.DataFrame:
    data = []
    for i, row in enumerate(occupancy_grid):
        for j, pixel in enumerate(row):
            if pixel > 0:
                data.append({"x": i, "y": j, "hue": 1})

    return pd.DataFrame.from_records(data)


# xrange = np.arange(x, max_x, x_dir * 1).round().astype(int)
def arange_as_ints(
    start: Union[int, float],
    stop: Union[int, float],
    step: Union[int, float],
    max_num_points: int = None,
) -> np.ndarray:
    """
    Generates a range of values from min to max incrementing by step. If max_num_points
    is specified then the range will be truncated to max_num_points. Also, each point will
    be rounded to the nearest int.

    Example:
        arange_as_ints(0, 3, 0.4, max_num_points=6)

        # np.arange would produce: [0, 0.4, 0.8, 1.2, 1.6, 2, 2.4, 2.8] \n
        # arange_as_ints would produce: [0, 0, 1, 1, 2, 2]


    Args:
        start (int | float): where to start range from
        stop (int | float): where to end range at
        step (int | float): increment
        max_num_points (int, optional): _description_. Defaults to None.

    Returns:
        np.ndarray: Array containing the range of values
    """
    _stop = stop

    if max_num_points is not None:
        if step > 0:
            _stop = min(start + max_num_points * step, stop)
        else:
            _stop = max(start + max_num_points * step, stop)

    return np.arange(start, _stop, step).round().astype(int)
