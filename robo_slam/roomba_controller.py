from enum import Enum
from random import random
from typing import Optional, Tuple, cast

import numpy as np
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from rclpy.publisher import Publisher
from sensor_msgs.msg import LaserScan

from .robo_utils import *


class ThymStates(Enum):
    # Waiting until we've gotten at least 1 range and 1 odometry message before starting
    INITIALIZING = 0

    # Move straight until an obstacle is detected or a timer is exceeded
    MOVING = 1

    # Do slow sweep until timer exceeded
    SWEEP = 2

    # Turn away from obstacle
    TURNAWAY = 3


class RoombaController:
    """
    A roomba controller that will move in a straight line until an obstacle is detected. It will then
    turn away from the wall at some random angle in the range [angle_low, angle_high].

    Example:

    self
    """

    def __init__(
        self,
        velocity_publisher: Publisher,
        sensor_threshold: float = 0.1,
        linear_velocity: float = 0.2,
        angle_range: Tuple[float, float] = (30, 150),
        p_turnaway: float = 10,
    ):
        """
        A roomba controller that will move in a straight line until an obstacle is detected. It will then
        turn away from the wall at some random angle in the range [angle_low, angle_high].

        Args:
            velocity_publisher: publisher to publish velocity commands to. should publish Twist type.

            sensor_threshold: distance from wall in meters that will trigger a stop and turn

            linear velocity: linear velocity (m/s) to use when moving

            angle_range: (angle_low, angle_high). Range of angles in degrees that the roomba can turn away
            from an obstacle
            p_turnaway (float): proportional controller parameter for turning away from an obstacle
        """
        # publisher to publish velocity commands to
        self._velocity_publisher = velocity_publisher

        # helpful constants
        self._DIRECTION_CW = -1.0
        self._DIRECTION_CCW = 1.0

        # initializing parameters that can be used to tune the Thymio Roomba performance
        self._sensor_threshold = sensor_threshold
        self._p_turnaway = p_turnaway
        self._angle_low, self._angle_high = angle_range
        self.linear_velocity = linear_velocity

        # wait for first odometry and range scanner message
        self._current_state = ThymStates.INITIALIZING

        self._turnaway_angle: Optional[float] = None

        self._odometry: Optional[Odometry] = None
        self._laser_scan: Optional[LaserScan] = None

    def _state_moving(self):
        """
        Simple state. Moves forward until an obstacle is detected.
        """
        # move forward unless an obstacle is detected
        cmd_vel = Twist()
        cmd_vel.linear.x = self.linear_velocity
        self._velocity_publisher.publish(cmd_vel)

        # detecting obstacle. stopping and moving to turnaway state if detected
        for range in self._laser_scan.ranges:
            if range >= 0 and range <= self._sensor_threshold:
                cmd_vel = Twist()
                self._velocity_publisher.publish(cmd_vel)

                self._current_state = ThymStates.TURNAWAY

    def _state_initializing(self):
        """
        State moves to moving state if we have received at least 1 laser scan and 1 odometry scan
        """
        if self._odometry is not None and self._laser_scan is not None:
            self._current_state = ThymStates.MOVING

    def calculate_turnaway_angle(self) -> float:
        """
        Calculating an angle to turn away from an obstacle. Note this is in reference to the
        thymios origin and not the odometry frame of reference.

        Returns:
            float: angle to turn away, in the range [angle_low, angle_high], from thymio origin
        """
        # generating random turnaway angle between 30 and 180 degrees
        # taking abs value because max proximity is ~0.14 and no proximity
        # (distance too far for sensor) is -1.0. Taking the absolute value
        # allows us to consider obstacle too far to detect as larger than a
        # valid sensor value.
        ranges = self._laser_scan.ranges if self._laser_scan is not None else [-1.0]
        left = abs(ranges[1])
        right = abs(ranges[-1])

        # if the right sensor is farther than the left sensor, rotating right will
        # rotate away from the wall
        if right > left:
            direction = self._DIRECTION_CW
        else:
            direction = self._DIRECTION_CCW

        # choosing a random angle between self.angle_low and self.angle_high
        turn_angle = direction * np.deg2rad(
            random() * (self._angle_high - self._angle_low) + self._angle_low
        )

        return turn_angle

    def _state_turnaway(self):
        """
        State to control angular velocity used to turn away from an obstacle.
        """
        current_pose = pose3d_to_2d(self._odometry.pose.pose)

        # determining how much to turn away from the obstacle
        if self._turnaway_angle is None:
            turnaway_angle = self.calculate_turnaway_angle()
            self._turnaway_angle = cast(float, current_pose.theta + turnaway_angle)

        # using proportional controller to rotate to desired angle
        angle_error = angle_difference(self._turnaway_angle, current_pose.theta)
        if abs(angle_error) >= 0.1:
            cmd_vel = Twist()
            cmd_vel.angular.z = self._p_turnaway * angle_error
            self._velocity_publisher.publish(cmd_vel)

        else:
            self._turnaway_angle = None
            self._current_state = ThymStates.MOVING

    def _state_sweep(self):
        vel = Twist()
        vel.angular.z = 0.6

        self._velocity_publisher.publish(vel)

    def _update_callback(self, event=None):
        # todo terrible, fix this
        if self._current_state == ThymStates.MOVING and event == "timer":
            self._current_state = ThymStates.SWEEP

        elif self._current_state == ThymStates.SWEEP and event == "timer":
            self._current_state = ThymStates.MOVING

        if self._current_state == ThymStates.INITIALIZING:
            self._state_initializing()

        if self._current_state == ThymStates.MOVING:
            self._state_moving()

        if self._current_state == ThymStates.SWEEP:
            self._state_sweep()

        elif self._current_state == ThymStates.TURNAWAY:
            self._state_turnaway()

    def step_odometry(self, odom: Odometry):
        """
        Step function for odometry. To be called in the callback of an odometry message.
        """
        self._odometry = odom
        self._update_callback()

    def step_laserscan(self, laser_scan: LaserScan):
        """
        Step function for a laser scanner. Should be called in the callback of a laser scan message.

        Args:
            laser_scan (LaserScan): _description_
        """
        self._laser_scan = laser_scan
        self._update_callback()

    def step_timer(self):
        self._update_callback("timer")

    def stop(self):
        """
        Stop running and reset the state machine.
        """
        # Set all velocities to zero
        cmd_vel = Twist()
        self._velocity_publisher.publish(cmd_vel)

        # reset state machine and sensor readings
        self._current_state = ThymStates.INITIALIZING
        self._odometry = None
        self._laser_scan = None
