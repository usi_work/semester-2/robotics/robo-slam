import sys
from typing import Optional

import rclpy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from rclpy.node import Node
from sensor_msgs.msg import LaserScan

from .robo_utils import *
from .roomba_controller import RoombaController


class Robom(Node):
    def __init__(self):
        super().__init__("robom_controller")  # type: ignore

        self.last_odom: Optional[Odometry] = None

        self.create_subscription(Odometry, "/robo1/odom", self.odom_callback, 10)
        self.create_subscription(
            LaserScan, "/robo1/laser_scanner", self.laserscan_callback, 10
        )
        self.timer = self.create_timer(10, self.timer_callback)

        self.vel_pub = self.create_publisher(Twist, "/robo1/cmd_vel", 10)

        self.roomba_controller = RoombaController(
            self.vel_pub,
            sensor_threshold=0.2,
            linear_velocity=0.2,
            angle_range=(30, 150),
            p_turnaway=1,
        )

    def timer_callback(self):
        self.roomba_controller.step_timer()

    def odom_callback(self, msg: Odometry):
        self.roomba_controller.step_odometry(msg)

    def laserscan_callback(self, msg: LaserScan):
        self.roomba_controller.step_laserscan(msg)

    def start(self):
        cmd_vel = Twist()
        cmd_vel.angular.z = 0.2

        self.vel_pub.publish(cmd_vel)

    def stop(self):
        self.vel_pub.publish(Twist())


def main():
    # Initialize the ROS client library
    rclpy.init(args=sys.argv)

    # Create an instance of your node class
    node = Robom()

    # node.start()

    # Keep processings events until someone manually shuts down the node
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    node.stop()


if __name__ == "__main__":
    main()
