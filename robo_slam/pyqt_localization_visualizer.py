from multiprocessing import Queue

import numpy as np
import pyqtgraph as pg
from PyQt5 import QtCore, QtWidgets

from .robo_utils import occupancy_grid_to_scatter


def interleave_arrays(a: np.ndarray, b: np.ndarray):
    """
    Interleave 2 1-D arrays.

    ex)
    interleave_arrays(np.array([1, 3, 5]), np.array([2, 4, 6]))
    [1,2,3,4,5,6]

    Args:
        a (np.ndarray): 1-dimensional numpy array
        b: (np.ndarray): 1-dimensional numpy array
    """
    c = np.empty(a.shape[0] + b.shape[0])
    c[0::2], c[1::2] = a, b

    return c


class PyQtLocalizationVisualizer(QtWidgets.QMainWindow):
    def __init__(
        self,
        groundtruth_queue: Queue,
        particle_queue: Queue,
        estimate_queue: Queue,
        occupancy_grid,
    ):
        super(PyQtLocalizationVisualizer, self).__init__()

        # plot configurations
        # length of orientation lines for ground truth and particles
        self.line_length = 15

        # shared queues between ros node and pyqt graph
        self.groundtruth_queue = groundtruth_queue
        self.particle_queue = particle_queue
        self.estimate_queue = estimate_queue

        # setting plot width and height based on our occupancy grid size
        self.plot_width = occupancy_grid.shape[0]
        self.plot_height = occupancy_grid.shape[1]

        # initializing the plot data
        self._init_occupancy_grid(occupancy_grid)
        self._init_groundtruth()
        self._init_particles()
        self._init_estimate()

        # adding plot data to our plot widget
        plot = self._create_plots()
        self._init_layout(plot)

        # periodically check plot queues for data and update plot
        self.timer = QtCore.QTimer()
        self.timer.setInterval(50)
        self.timer.timeout.connect(self._update_plot_data)
        self.timer.start()

    def _init_layout(self, plot):
        """
        Setup our qt graph application layout.

        Args:
            plot (_type_): QT Plot widget
        """
        widget = QtWidgets.QWidget()
        layout = QtWidgets.QGridLayout()

        widget.setLayout(layout)
        layout.addWidget(plot, 0, 1, 3, 1)

        self.setCentralWidget(widget)

    def _init_occupancy_grid(self, occupancy_grid):
        """
        Initializing the occupancy grid

        Args:
            occupancy_grid (_type_): 2D array with each cell with 0 meaning free and
                                     any other number meaning occupied
        """
        df_scatter = occupancy_grid_to_scatter(occupancy_grid)

        self.occupancy_grid_plot = pg.ScatterPlotItem(
            size=10, brush=pg.mkBrush(0, 0, 0, 255)
        )
        self.occupancy_grid_plot.addPoints(x=df_scatter["x"], y=df_scatter["y"])

    def _init_groundtruth(self):
        """
        Setup our ground truth position and orientation plot. This will show the actual position of the robot
        """
        self.groundtruth_position_plot = pg.ScatterPlotItem(
            size=10, brush=pg.mkBrush(color="r")
        )
        self.groundtruth_orientation_plot = pg.PlotDataItem(
            pen=pg.mkPen(width=2.5, color="r")
        )

        # always show ground truth on top
        self.groundtruth_position_plot.setZValue(1)
        self.groundtruth_orientation_plot.setZValue(1)

    def _init_estimate(self):
        """
        Setup our estimate position plot. This will show the estimated position generated from the MCL algorithm.
        """
        self.estimate_position_plot = pg.ScatterPlotItem(
            size=8, brush=pg.mkBrush(color="g")
        )

        # always show estimate on top, even above ground truth
        self.estimate_position_plot.setZValue(2)
        self.estimate_position_plot.setZValue(2)

    def _init_particles(self):
        """
        Setup particles to be drawn as a circle to mark location with a line coming out for orientation
        """
        self.particle_position_plot = pg.ScatterPlotItem(
            size=10,
            brush=pg.mkBrush(0, 0, 255, 25),
            pen=pg.mkPen(width=2.5, color=(0, 0, 0, 0)),
        )
        self.particle_orientation_plot = pg.PlotDataItem(
            pen=pg.mkPen(width=2.5, color=(0, 0, 255, 50))
        )

    def _create_plots(self):
        """
        Adding ground truth, particles and position estimate to our plot widget
        """
        plot = pg.plot()

        plot.setXRange(0, self.plot_width)
        plot.setYRange(0, self.plot_height)

        plot.addItem(self.occupancy_grid_plot)
        plot.addItem(self.groundtruth_orientation_plot)
        plot.addItem(self.groundtruth_position_plot)
        plot.addItem(self.particle_position_plot)
        plot.addItem(self.particle_orientation_plot)
        plot.addItem(self.estimate_position_plot)

        plot.setBackground("w")

        return plot

    def _update_groundtruth(self):
        """
        Updates ground truth position and orientation. Note this should note be called when
        the groundtruth queue is empty.
        """
        # read from ground truth queue
        (x, y, theta) = self.groundtruth_queue.get()

        # generate an orientation line
        xoffset = x + self.line_length * np.cos(theta)
        yoffset = y + self.line_length * np.sin(theta)
        self.groundtruth_orientation_plot.setData(
            x=[x, xoffset], y=[y, yoffset], connect="pairs"
        )

        # update ground truth marker
        self.groundtruth_position_plot.clear()
        self.groundtruth_position_plot.addPoints(x=[x], y=[y])

    def _update_particles(self):
        """
        Updates particle position and orientation. Note this should note be called when
        the particle queue is empty.
        """
        # reading particle poses from queue
        poses = self.particle_queue.get()
        x, y = poses[:, 0], poses[:, 1]

        # update particle markers
        self.particle_position_plot.clear()
        self.particle_position_plot.addPoints(x=poses[:, 0], y=poses[:, 1])

        # create an orientation line for each particle
        xoffset = x + self.line_length * np.cos(poses[:, 2])
        yoffset = y + self.line_length * np.sin(poses[:, 2])

        # connect="pairs" connects every other point, so we interleave x with xoffset and y with yoffset
        # so that all points in x,y are connected to the corresponding point in xoffset, yoffset
        # ie) x[0],y[0] connected to xoffset[0], yoffset[0]
        xdraw = interleave_arrays(x, xoffset)
        ydraw = interleave_arrays(y, yoffset)
        self.particle_orientation_plot.setData(x=xdraw, y=ydraw, connect="pairs")

    def _update_estimate(self):
        # read from estimate queue
        (x, y) = self.estimate_queue.get()

        # update estimate marker
        self.estimate_position_plot.clear()
        self.estimate_position_plot.addPoints(x=[x], y=[y])

    def _update_plot_data(self):
        if self.groundtruth_queue.empty() == False:
            self._update_groundtruth()

        if self.particle_queue.empty() == False:
            self._update_particles()

        if self.estimate_queue.empty() == False:
            self._update_estimate()
