import numpy as np
import rclpy
from geometry_msgs.msg import Pose2D
from rclpy.node import Node
from robo_interfaces.msg import Pose2DArray
from sklearn.cluster import DBSCAN


class RoboLocalizationEstimation(Node):
    def __init__(self):
        super().__init__("robo_localization_estimation")  # type: ignore

        self.particle_subscriber = self.create_subscription(
            Pose2DArray, "/particles", self.estimate_cb, 10
        )
        self.estimate_publisher = self.create_publisher(Pose2D, "/estimate", 10)
        self.dbscanner = DBSCAN(eps=10, min_samples=10)

    def estimate_cb(self, msg: Pose2DArray):
        """
        Estimate the pose of the robot given a set of particles.

        Args:
            msg (Pose2DArray): Particles generated from MCL
        """
        # choose the top particles and send an estimated pose
        pose = Pose2D()

        # using the dbscan clustering algorithm to generate clusters of particles.
        poses = np.array([[pose.x, pose.y] for pose in msg.poses])
        clustering = self.dbscanner.fit(poses)

        # taking the centroid of the cluster with the most particles as the estimate
        arg = np.bincount(clustering.labels_ + 1).argmax() - 1
        blob = [pose for i, pose in enumerate(poses) if clustering.labels_[i] == arg]
        pose.x, pose.y = np.mean(blob, axis=0)

        self.estimate_publisher.publish(pose)


def main():
    rclpy.init()

    node = RoboLocalizationEstimation()

    # Keep processings events until someone manually shuts down the node
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
