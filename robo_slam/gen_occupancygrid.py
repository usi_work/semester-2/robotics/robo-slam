import sys

import numpy as np
import pandas as pd
import rclpy
import seaborn as sns
from cv_bridge import CvBridge
from rclpy.node import Node
from sensor_msgs.msg import Image
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
from rclpy.task import Future


class GenerateOccupancyGrid(Node):
    def __init__(self):
        super().__init__("occupancygrid_generator")  # type: ignore
        self.cv_bridge = CvBridge()

        self.create_subscription(Image, "/image", self.image_callback, 10)
        self._image_capture_complete = None

    def image_callback(self, msg: Image):
        data = self.cv_bridge.imgmsg_to_cv2(msg, "bgr8")

        occupancy_grid = np.ndarray(shape=(msg.width, msg.height), dtype=np.uint8) * 0
        for i, row in enumerate(data):
            for j, pixel in enumerate(row):
                occupancy_grid[i, j] = pixel[0]

        if self._image_capture_complete is not None:
            self._image_capture_complete.set_result(occupancy_grid)

    def capture_image(self) -> Future:
        self._image_capture_complete = Future()
        return self._image_capture_complete


def occupancy_grid_to_scatter(occupancy_grid: np.ndarray) -> pd.DataFrame:
    data = []
    for i, row in enumerate(occupancy_grid):
        for j, pixel in enumerate(row):
            if pixel > 0:
                data.append({"x": i, "y": j, "hue": 1})

    return pd.DataFrame.from_records(data)


def main():
    # Initialize the ROS client library
    rclpy.init(args=sys.argv)

    # Create an instance of your node class
    node = GenerateOccupancyGrid()

    # node.start()
    capture_image_future = node.capture_image()

    # capture image and plot
    try:
        rclpy.spin_until_future_complete(node, capture_image_future)
        occupancy_grid: np.ndarray = capture_image_future.result()
        print("Plotting occupancy grid")
        cmap = ListedColormap(["black"])
        # sns.heatmap(occupancy_grid, cmap=cmap, square=True, cbar=False)
        df_scatter = occupancy_grid_to_scatter(occupancy_grid)
        sns.scatterplot(
            data=df_scatter, x="x", y="y", hue="hue", palette=["black"], legend=False
        )
        plt.show()

        print("Writing occupancy grid to file")
        pd.DataFrame(occupancy_grid).to_csv(
            "occupancy_grid.csv", header=None, index=None
        )
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
