from glob import glob
from setuptools import setup

package_name = "robo_slam"

setup(
    name=package_name,
    version="0.0.0",
    packages=[package_name],
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name + "/launch", glob("launch/*.launch.py")),
        ("share/" + package_name, ["package.xml"]),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="usi",
    maintainer_email="jromeo1982@gmail.com",
    description="TODO: Package description",
    license="TODO: License declaration",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "robo_slam = robo_slam.robo_slam:main",
            "robo_localization = robo_slam.robo_localization:main",
            "robo_localization_visualizer = robo_slam.robo_localization_visualizer:main",
            "robo_localization_estimation = robo_slam.robo_localization_estimation:main",
        ],
    },
)
