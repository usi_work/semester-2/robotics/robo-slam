# Quickstart

The following steps assume that the robomaster_ros and robomaster_sim are are already up to date.

## Clone Repos

Clone the repos. Note renaming the repo is important because ROS won't build nodes with dashes in them.

```bash
cd ~/dev_ws/src
git clone git@gitlab.com:usi_work/semester-2/robotics/robo-slam.git robo_slam
git clone git@gitlab.com:usi_work/semester-2/robotics/robo_interfaces.git
```

## Install Dependencies

I believe I captured all the additional libraries I used on top of the
the base image. I apologize if I missed any.

```bash
# these are definitely required by the nodes run from the roboslam launch file
pip install sklearn
pip install pyqtgraph

# these are only needed by test scripts that you probably will not run
pip install seaborn
pip install shapely
```

## Patch simExtROS2

The simExtROS2 requires that the meta/interfaces.txt file be updated if more ROS interfaces are needed.
To add the missing interfaces required by this module run the following steps from the command line:

```bash
cd ~/dev_ws/src/robo_slam
./patch_sim.sh
```

## Rebuild Workspace

```bash
cd ~/dev_ws
colcon build
```

## Start Coppelia Simulation

```bash
~/apps/CoppeliaSim_Edu_V4_3_0_Ubuntu20_04/coppeliaSim.sh
```

## Open Scene File

Once Coppelia is open, we then need to open the scene file. The relevant scene file can be found at
/home/usi/dev_ws/src/robo_slam/scenes/playground_tof_ep_rangescanner_extra_wall.ttt.  You should see
a scene similar to the one shown in the picture below:

<img src="./documentation/imgs/coppelia_scene.png" alt="coppelia_scene" style="width:500px;"/>

From here start the simulation by hitting the button highlighted in red on the left in the image above.
Ensure that real time mode is highlighted (the button highlighted in red on the right).

## Start Robomaster ROS Node

Next we need to start the robomaster ros node in a fresh terminal. This will make the odometry and cmd velocity
topics available.

```bash
cd ~/dev_ws
source ~/dev_ws/install/setup.bash
ros2 launch robomaster_ros ep.launch name:=robo1 chassis_rate:=20
```

## Start Roboslam Package

Next we use the roboslam launch file to kick off the various nodes needed to perform Monte Carlo Localization.

```bash
cd ~/dev_ws
source ~/dev_ws/install/setup.bash
ros2 launch robo_slam robo_slam.launch.py
```

If everything works correctly the robomaster should start moving and a visualization should pop up showing the
Monte Carlo Localization algorithm in action. That should look something like the image below:

<img src="./documentation/imgs/sim_and_visualization.png" alt="sim_and_visualization" style="width:500;"/>

The algorithm should converge on the correct location at some time that can range anywhere from a few seconds
to a few minutes depending on the initial particle guesses.

# Generating Occupancy Grid

The occupancy grid for the given scene has already been generated, but you can use the following steps to regenerate
if needed. First start Coppelia, open the scene as described in the quickstart guide. Then unfortunately for some reason
the robomaster ros node seems to change the resolution of the vision sensor which causes issues. Ensure that the robomaster
ros node is not running, and then double check that the resolution for the vision sensor is set to 500 x 500 cm.
See the 2 images below for examples of how to do that.

<img src="./documentation/imgs/fix_vision_sensor_0.png" alt="sim_and_visualization" style="width:250;"/>
<img src="./documentation/imgs/fix_vision_sensor_1.png" alt="sim_and_visualization" style="width:250;"/>

Next you need to hide the robomaster from the scene so it is not visible in the generated occupancy grid.

<img src="./documentation/imgs/hide_robomaster.png" alt="sim_and_visualization" style="width:600;"/>

Once the vision sensor is configured with the correct resolution and the robomaster hidden, you can start the simulation as described in the quickstart guide.
After the simulation is up and running, you can simply run the following python script to generate the occupancy grid
CSV file.

```bash
source ~/dev_ws/install/setup.bash
cd ~/dev_ws/src/robo_slam/robo_slam
python3 gen_occupancygrid.py
```
